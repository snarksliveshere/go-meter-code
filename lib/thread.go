package lib

import (
	"fmt"
	"net/url"
	"os"
	"time"

	"github.com/a696385/go-meter/http"
)

func NewThread(config *Config) {
	timerAllow := time.NewTicker(time.Duration(250) * time.Millisecond)
	allow := int32(config.MRQ / 4 / config.Threads)
	if config.MRQ == -1 {
		allow = -1
	} else if allow <= 0 {
		allow = 1
		timerAllow.Stop()
		timerAllow.C = nil
	}
	currentAllow := allow
	for {
		select {
		//Set allow requests per time timer
		case <-timerAllow.C:
			currentAllow = allow
		//Get free tcp connection
		case connection := <-config.connectionManager.C:
			if config.MRQ != -1 {
				currentAllow--
			}
			//Return connection to pool if allowed request is 0
			if currentAllow > 0 || config.MRQ == -1 {
				connection.Take()
				//Create request object
				req := getRequest(config)
				//Send request if we connected
				go connection.Exec(req, config.requestStats)
			} else {
				connection.Return()
			}
		//Wait exit event
		case <-config.workerQuit:
			//Complete exit
			config.workerQuited <- true
			return
		}
	}
}

func getRequest(c *Config) *http.Request {
	if c.Method == "POST" || c.Method == "PUT" {
		return &http.Request{
			Method:        c.Method,
			URL:           c.URL,
			Header:        c.Header,
			Body:          c.Data,
			ContentLength: int64(len(c.Data)),
			Host:          c.URL.Host,
		}
	} else {
		//Use source data as URL request or original URL
		var (
			r   *url.URL
			err error
		)
		if c.Data != nil {
			r, err = url.Parse(string(c.Data))
			if err != nil {
				fmt.Printf("ERROR: URL is broken %s\n", string(c.Data))
				os.Exit(1)
			}
		} else {
			r = c.URL
		}
		return &http.Request{
			Method: c.Method,
			URL:    r,
			Header: c.Header,
			Host:   c.URL.Host,
		}
	}
}
