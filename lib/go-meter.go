package lib

import (
	"fmt"
	"net/url"
	"os"
	"os/signal"
	"runtime"
	"runtime/pprof"
	"strings"
	"syscall"
	"time"
)

type RequestStats struct {
	ResponseCode int
	Duration     time.Duration
	NetIn        int64
	NetOut       int64
}

type Config struct {
	Method            string
	Header            map[string][]string
	URL               *url.URL
	Data              []byte
	Connections       int
	Threads           int
	MRQ               int
	Verbose           bool
	ExcludeSeconds    time.Duration
	source            *Source
	Duration          time.Duration
	connectionManager *ConnectionManager
	workerQuit        chan bool
	workerQuited      chan bool
	statsQuit         chan bool
	statsQuited       chan bool
	requestStats      chan *RequestStats

	WriteCPUProfileToFile string
}

func Start(cfg Config) {
	var (
		sourceData *Source
	)

	cfg.Method = strings.ToUpper(cfg.Method)

	if cfg.Method == "POST" || cfg.Method == "PUT" || len(cfg.Data) > 0 {
		sourceData = &Source{
			Data: cfg.Data,
		}
	} else {
		sourceData = &Source{}
	}

	if cfg.WriteCPUProfileToFile != "" {
		f, err := os.Create(cfg.WriteCPUProfileToFile)
		if err != nil {
			fmt.Printf("Can not start cpu proffile %v\n", err)
			return
		}
		pprof.StartCPUProfile(f)
		defer pprof.StopCPUProfile()
	}

	if cfg.MRQ == 0 {
		cfg.MRQ = -1
	}

	config := &Config{
		Data:           cfg.Data,
		Header:         cfg.Header,
		Method:         cfg.Method,
		URL:            cfg.URL,
		Connections:    cfg.Connections,
		Threads:        cfg.Threads,
		MRQ:            cfg.MRQ,
		Verbose:        cfg.Verbose,
		ExcludeSeconds: cfg.ExcludeSeconds,
		source:         sourceData,
		Duration:       cfg.Duration,
		workerQuit:     make(chan bool, cfg.Threads),
		workerQuited:   make(chan bool, cfg.Threads),
		statsQuit:      make(chan bool, 1),
		statsQuited:    make(chan bool, 1),
		requestStats:   make(chan *RequestStats, cfg.Connections*512),
	}

	runtime.GOMAXPROCS(cfg.Threads)

	logUrl := config.URL.String()
	if cfg.Method != "POST" && cfg.Method == "PUT" {
		logUrl = config.URL.Host
	}

	if config.MRQ == -1 {
		fmt.Printf("Running test threads: %d, connections: %d in %v %s %s\n", cfg.Threads, config.Connections, config.Duration, config.Method, logUrl)
	} else {
		fmt.Printf("Running test threads: %d, connections: %d, max req/sec: %d, in %v %s %s\n", cfg.Threads, config.Connections, config.MRQ, config.Duration, config.Method, logUrl)
	}

	config.connectionManager = NewConnectionManager(config)

	//check any connect
	anyConnected := false
	for i := 0; i < config.Connections && !anyConnected; i++ {
		connection := config.connectionManager.conns[i]
		if connection.IsConnected() {
			anyConnected = true
		}
	}
	if !anyConnected {
		fmt.Printf("Can not connect to %s\n", config.URL.Host)
		return
	}

	go StartStatsAggregator(config)

	for i := 0; i < config.Threads; i++ {
		go NewThread(config)
	}

	//Start SIGTERM listen
	signalChan := make(chan os.Signal, 1)
	signal.Notify(signalChan, os.Interrupt, syscall.SIGTERM)

	//Wait timers or SIGTERM
	select {
	case <-time.After(config.Duration):
	case <-signalChan:
	}
	for i := 0; i < config.Threads; i++ {
		config.workerQuit <- true
	}
	//Wait for threads complete
	for i := 0; i < config.Threads; i++ {
		<-config.workerQuited
	}

	//Stop stats aggregator
	config.statsQuit <- true
	//Close connections
	for i := 0; i < config.Connections; i++ {
		connection := config.connectionManager.conns[i]
		if !connection.IsConnected() {
			continue
		}
		connection.conn.Close()
	}
	//Wait stats aggregator complete
	<-config.statsQuited
	//Print result
	PrintStats(os.Stdout, config)
}
